1. Target of project : 
- Read GPS data (.text)
- Filter noise data
- Convert data to (.csv)
2. How to run project
- Import project with type : Java Maven Project
- Run project with class : Filter.java (package com.ioit.secondo)
- If want to change input file (GPS data) : Change at class : Filter.java (package com.ioit.secondo), method : main
	String fileName = "???";
- If want to change out put file (CSV data) : Change at class : Filter.java (package com.ioit.secondo), method : main
	Flume.flume(vehicleGPSList, "???");
3. Utility
- Some methors for filter noise, you can view at class  Filter.java (package com.ioit.secondo)